CREATE TABLE IF NOT EXISTS eventos(
    id INTEGER PRIMARY KEY AUTOINCREMENT, 
     nombre TEXT, 
     direccion TEXT,
     description TEXT, 
     horario TEXT, 
     foto TEXT );

INSERT INTO eventos
    (nombre, direccion, description, horario,foto) VALUES 
    ('Actuacion agrupaciones locales', 'Auditorio Jose Toro Prado del rey (Cadiz)', 'Chirigota:Vaya Pitorreo','21:00', 'assets/imgs/chirininas.jpg');
INSERT INTO eventos
    (nombre, direccion, description, horario,foto) VALUES 
    ('Actuacion agrupaciones locales', 'Auditorio Jose Toro Prado del rey (Cadiz)', 'Chirigota:Si me quereis irse','21:00', 'assets/imgs/chirininos.jpg');
