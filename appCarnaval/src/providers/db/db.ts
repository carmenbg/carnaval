
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { Http } from '@angular/http';
import { Platform, AlertController } from 'ionic-angular';
import { BehaviorSubject } from 'rxjs/Rx';

/*
  Generated class for the DbProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DbProvider {

  db : SQLiteObject = null;
  eventos : any;

  // Array donde se almacenará la información contenida en la BD
   datos: any = [];
   // Variable que se usará con subscribe para detectar BD preparada 
   private databaseReady: BehaviorSubject<boolean>;

  constructor(public http: Http, private platform: Platform, private sqlite: SQLite, private sqliteporter: SQLitePorter, 
    private alertCtrl: AlertController) {
    
    this.databaseReady = new BehaviorSubject(false);
    this.abrirBaseDatos();

  }

 public abrirBaseDatos() {
    // Make ready to platform
    this.platform.ready()
      .then(() => {
        // Crear o abrir la base de datos MiAppBD.db
        this.sqlite.create({
          name: 'MiAppBD.db',
          location: 'default'
        })
          .then((db: SQLiteObject) => {
            this.db = db;
            // Consulta para comprobar si ya existen los datos
            db.executeSql('SELECT * FROM eventos', [])
              .then(res => {
              /*  // Los datos ya estaban en la BD
                this.alertCtrl.create({
                  title: 'Información',
                  message: 'Los datos ya estaban en la BD',
                  buttons: ['Aceptar']
                     }).present();*/
                // Procesar los datos de la base de datos
                this.datos = this.obtenerDatos(db);
                // Informar que la base de datos está lista
                this.databaseReady.next(true);
              }) 
              .catch(res => {
                // Los datos no están en la BD. Hay que importarlos
                this.alertCtrl.create({
                  title: 'Información',
                  message: 'Se van a importar los datos a la BD',
                  buttons: ['Aceptar']
                }).present();
                // Obtener el archivo que contiene las sentencias SQL
                this.http.get('assets/baseDatosLocal.sql')
                  .map(res => res.text())
                  .subscribe(sql => {
                    // Ejecutar las sentencias SQL del archivo
                    this.sqliteporter.importSqlToDb(db, sql)
                      .then(() => {
                        // Procesar los datos de la base de datos
                        this.datos = this.obtenerDatos(db);
                        // Informar que la base de datos está lista
                        this.databaseReady.next(true);
                      }).catch(e => {
                        alert("Error al importar la base de datos");
                        console.error("Error al importar la base de datos", e.message);
                      });
                  })
              });
          });
      }).catch(e => alert('Platform is not ready.'));    
  }

  public obtenerDatos(baseDatosLocal) {
    let resultado = [];
    // Realizar la consulta a la BD para extraer los datos
    baseDatosLocal.executeSql('SELECT * FROM eventos', [])
      .then(resSelect => {
        // Recorrer todas las filas obtenidas
        for (var i = 0; i < resSelect.rows.length; i++) { 
          // Añadir al array de datos la información desglosada
          resultado.push({ 
            nombre: resSelect.rows.item(i).nombre, 
            direccion: resSelect.rows.item(i).direccion,
            description: resSelect.rows.item(i).description,
            horario: resSelect.rows.item(i).horario,
            foto:  resSelect.rows.item(i).foto,
          });
        } 
      }).catch(e => {
        alert("Error: No se ha podido consultar los datos",);
        console.error("Error en Select de consulta de datos", e.message);
      });
    return resultado;
  }
  
  getDatabaseState() {
    return this.databaseReady.asObservable();
  }

  public addEvento(evento){
    let sql = "INSERT INTO eventos (nombre, direccion, description, horario, foto) values (?,?,?,?,?)";
    return this.db.executeSql(sql,[evento.nombre,evento.direccion,evento.description,evento.horario, evento.foto]);
  }

  public getEventos(){
    let sql = "SELECT * FROM eventos";
    return this.db.executeSql(sql,{});
  }
  public borrarEvento(id){
    let sql = "DELETE FROM eventos WHERE id= ? ";
    return this.db.executeSql(sql,[id]);
 }

 public modificaEvento(evento){
  //console.log("modifica");
  let sql = "UPDATE eventos  SET nombre = ?, direccion = ?, description = ?, horario = ?, foto = ? WHERE id = ? ";
  return this.db.executeSql(sql,[evento.nombre,evento.direccion,evento.description,evento.horario, evento.foto,evento.id]);
}

}
