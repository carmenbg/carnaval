import { Component } from '@angular/core';
import { NavController,ModalController,NavParams } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  eventos : any;
  datos: any = [];  

  nombre : string;
  direccion: string;
  description: string ;
  horario : any='';
  foto: any = '';
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
  public db : DbProvider,public modalCtrl : ModalController,public alertCtrl : AlertController)  {
    
    let suscripcionBDPreparada =db.getDatabaseState()
    .subscribe(baseDatosPreparada => {
      if (baseDatosPreparada) {
        // Mantener una referencia a los datos como una propiedad de esta clase
        this.datos = db.datos;
        //
        this.db.getEventos().then((res)=>{
          this.eventos = [];
          for(var i = 0; i < res.rows.length; i++){
             this.eventos.push({
               id: res.rows.item(i).id,
               nombre: res.rows.item(i).nombre,
               direccion: res.rows.item(i).direccion,
               description: res.rows.item(i).description,
               horario: res.rows.item(i).horario,
               foto:  res.rows.item(i).foto
             });
          }
         },(err)=>{ /* alert('error al sacar de la bd'+err) */ })
        // Terminar la suscripción
        suscripcionBDPreparada.unsubscribe();
      }
    });
       
   }



  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }
 
  
  ionViewDidEnter(){
    
    // this.db.openDb().then((res)=>{
    // console.log('Ha estas en la principal');
     this.db.getEventos().then((res)=>{
     this.eventos = [];
     for(var i = 0; i < res.rows.length; i++){
        this.eventos.push({
          id: res.rows.item(i).id,
          nombre: res.rows.item(i).nombre,
          direccion: res.rows.item(i).direccion,
          description: res.rows.item(i).description,
          horario: res.rows.item(i).horario,
          foto: res.rows.item(i).foto
        });
     }
    },(err)=>{ /* alert('error al sacar de la bd'+err) */ })
   }
 
   //  )}
 
  

  
    
  nuevoEvento(){
    // aquí vamos a abrir la pagina para añadir nuestro sitio.
    this.navCtrl.push('ModalNuevoEventoPage'); 
   }

   muestraEvento(evento){
    this.navCtrl.push('DetallePage', evento); 
    
  }

   borrarEvento(id){
    
        let alert = this.alertCtrl.create({
          title: 'Confirmar borrado',
          message: '¿Estás seguro de que deseas eliminar este sitio?',
          buttons: [
            {
              text: 'No',
              role: 'cancel',
              handler: () => {
                  // Ha respondido que no así que no hacemos nada         
               }
            },
            {
              text: 'Si',
              handler: () => {
    
                  this.db.borrarEvento(id).then((res)=>{
                // Una vez borrado el sitio recargamos el listado
                  this.db.getEventos().then((res)=>{
                  this.eventos = [];
                  for(var i = 0; i < res.rows.length; i++){
                    this.eventos.push({
                      id: res.rows.item(i).id,
                      nombre: res.rows.item(i).nombre,
                      direccion: res.rows.item(i).direccion,
                      description: res.rows.item(i).description,
                      horario: res.rows.item(i).horario,
                      foto: res.rows.item(i).foto
                      });
                  }
    
                  },(err)=>{ /* alert('error al sacar de la bd'+err) */ })
    
                },(err)=>{ /* alert('error al borrar de la bd'+err) */ });
              }
            }
          ]
        });
    
        alert.present();
    
       }
}

