import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalNuevoEventoPage } from './modal-nuevo-evento';

@NgModule({
  declarations: [
    ModalNuevoEventoPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalNuevoEventoPage),
  ],
})
export class ModalNuevoEventoPageModule {}
