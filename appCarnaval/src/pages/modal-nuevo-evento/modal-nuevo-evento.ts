import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { Camera, CameraOptions } from '@ionic-native/camera';


/**
 * Generated class for the ModalNuevoEventoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-nuevo-evento',
  templateUrl: 'modal-nuevo-evento.html',
})
export class ModalNuevoEventoPage {
  nombre : string;
  direccion: string;
  description: string ;
  horario : any='';
  foto: any = '';
  

  constructor(public navCtrl: NavController, public navParams: NavParams,private db: DbProvider,
   private viewCtrl : ViewController, private camera: Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalNuevoEvento');
  // this.nombre = this.navParams.get('nombre');
   //this.foto = this.navParams.get('foto');
  }

  cerrarModal(){ 
    this.viewCtrl.dismiss();
  }
  
  sacarFoto(){
    let cameraOptions : CameraOptions = {
      quality: 50,
      encodingType: this.camera.EncodingType.JPEG, 
      targetWidth: 800,
      targetHeight: 600,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      correctOrientation: true
      }
      this.camera.getPicture(cameraOptions).then((imageData) => {
        // imageData is a base64 encoded string
        this.foto = "data:image/jpeg;base64," + imageData;
      }, (err) => {
          console.log(err);
      });
  }
  
  guardarEvento(){
    let evento = {
      nombre: this.nombre,
      direccion: this.direccion ,
      description: this.description,
      horario: this.horario,
      foto: this.foto
      }
      this.db.addEvento(evento).then((res)=>{
        this.cerrarModal();
         /*  alert('se ha introducido correctamente en la bd'); */
        },(err)=>{ /* alert('error al meter en la bd'+err) */ })
      }

}
